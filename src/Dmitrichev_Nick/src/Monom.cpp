#pragma once

#include "Monom.h"


TMonom::TMonom(int cval, int ival) 
{
	Coeff = cval;
	Index = ival;
}
TDatValue * TMonom::GetCopy()  // make a copy of the monom
{
	TDatValue * temp = new TMonom(Coeff, Index); 
	return temp;
}
void TMonom::SetCoeff(int cval) 
{ 
	Coeff = cval;
}
int   TMonom::GetCoeff(void)
{ 
	return Coeff;
}
void  TMonom::SetIndex(int ival)
{ 
	Index = ival; 
}
int   TMonom::GetIndex(void)
{ 
	return Index;
}
TMonom&  TMonom::operator=(const TMonom &tm)
{
	Coeff = tm.Coeff; 
	Index = tm.Index;
	return *this;
}
bool  TMonom::operator==(const TMonom &tm)const
{
	return (Coeff == tm.Coeff) && (Index == tm.Index);
}
bool  TMonom::operator<(const TMonom &tm)const
{
	return Index<tm.Index;
}
double  TMonom::calculate(double x, double y, double z)
{
	int indx = Index / 100;
	int indy = (Index % 100) / 10;
	int indz = Index % 10;
	return Coeff*pow(x, indx)*pow(y, indy)*pow(z, indz);
}
ostream&  operator<<(ostream &os, TMonom &tm)
{
	string str = "";
	if (tm.Coeff < 0)
		str += '-';
	else
		str += '+';
	str += to_string(abs(tm.Coeff));
	if (tm.Index != 0)
	{
		int indx = tm.Index / 100;
		int indy = (tm.Index % 100) / 10;
		int indz = tm.Index % 10;
		if (indx != 0)
			if (indx != 1)
				str += ("*x^" + to_string(indx));
			else
				str += "*x";
		if (indy != 0)
			if (indy != 1)
				str += ("*y^" + to_string(indy));
			else
				str += "*y";
		if (indz != 0)
			if (indz != 1)
				str += ("*z^" + to_string(indz));
			else
				str += "*z";
	}
	os << str << ' ';
	return os;
}
