#include <gtest/gtest.h>
#include "Monom/Monom.h"

TEST(TMonom, can_create_monom)
{
	ASSERT_NO_THROW(TMonom m1);
}

TEST(TMonom, can_create_monom_with_args)
{
	ASSERT_NO_THROW(TMonom m1(-18, 253));
}

TEST(TMonom, can_set_monom_coeff)
{
	TMonom m1;
	ASSERT_NO_THROW(m1.SetCoeff(15));
}

TEST(TMonom, can_get_monom_coeff)
{
	TMonom m1(16, 1);
	EXPECT_EQ(16, m1.GetCoeff());
}

TEST(TMonom, can_set_monom_index)
{
	TMonom m1;
	ASSERT_NO_THROW(m1.SetIndex(18));
}

TEST(TMonom, can_get_monom_index)
{
	TMonom m1(1, 3);
	EXPECT_EQ(3, m1.GetIndex());
}

TEST(TMonom, can_assign_monoms)
{
	TMonom m1(10, 3), m2(-10, 6);
	m2 = m1;
	EXPECT_TRUE((m1.GetCoeff() == m2.GetCoeff())&&(m1.GetIndex() == m2.GetIndex()));
}

TEST(TMonom, can_get_copy)
{
	TMonom m1(210, 567);
	TMonom * m2;
	m2 = (TMonom *)m1.GetCopy();
	EXPECT_TRUE((m1.GetCoeff() == m2->GetCoeff()) && (m1.GetIndex() == m2->GetIndex()));
}

TEST(TMonom, equal_monoms_are_equal)
{
	TMonom m1(10, 91), m2(10, 91);
	EXPECT_TRUE(m1 == m2);
}

TEST(TMonom, monom_with_less_index_is_less)
{
	TMonom m1(10, 91), m2(4, 101);
	EXPECT_TRUE(m1 < m2);
}