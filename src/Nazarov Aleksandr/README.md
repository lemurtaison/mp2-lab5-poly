# Лабораторная работа №5. Полиномы

## Цели и задачи

В рамках лабораторной работы ставится задача создания программных средств, поддерживающих эффективное представление полиномов и выполнение следующих операций над ними:

 - ввод полинома;
 - организация хранения полинома;
 - удаление введенного ранее полинома;
 - копирование полинома;
 - сложение двух полиномов;
 - вычисление значения полинома при заданных значениях переменных;
 - вывод.

Предполагается, что в качестве структуры хранения будут использоваться списки. В качестве дополнительной цели в лабораторной работе ставится также задача разработки некоторого общего представления списков и операций по их обработке. В числе операций над списками должны быть реализованы следующие действия:
 
 - поддержка понятия текущего звена;
 - вставка звеньев в начало, после текущей позиции и в конец списков;
 - удаление звеньев в начале и в текущей позиции списков;
 - организация последовательного доступа к звеньям списка (итератор).



##Условия и ограничения

При выполнении лабораторной работы использовались следующие основные предположения: 

 - Разработка структуры хранения должна быть ориентирована на представление полиномов от трех неизвестных. 
 - Степени переменных полиномов не могут превышать значения 9. 
 - Число мономов в полиномах существенно меньше максимально возможного количества (тем самым, в структуре хранения должны находиться только мономы с ненулевыми коэффициентами).

##План работы

 - Разработка структуры хранения списков.
 - Разработка структуры хранения полиномов.
 - Проверка работоспособности написанных классов с помощью Google Test Framework.
 - Разработка тестового приложения.
 
 Структура проекта будет выглядеть следующим образом:

![scheme](image/scheme.png)

 - TDatValue - абстрактный класс объектов-значений списка
 - TMonom - класс мономов
 - TRootLink - базовый класс для звеньев
 - TDatLink - класс для звеньев (элементов) списка с указателем на объект-значение
 - TDatList - класс линейных списков
 - THeadRing - класс циклических списков с заголовком
 - TPolinom - класс полиномов

##Используемые инструменты

 - Система контроля версий Git.
 - Фреймворк для написания автоматических тестов Google Test.
 - Среда разработки Microsoft Visual Studio 2017 Professional Edition.

##Разработка структуры хранения списков

###Класс TDatValue 
####Файл TDatValue.h

```C++
#pragma once

class TDatValue 
{
public:
	virtual TDatValue * GetCopy() = 0; // создание копии
	~TDatValue() {}
};

typedef TDatValue* PTDatValue;
```

###Класс TRootLink
####Файл TRootLink.h

```C++
#pragma once
#include "TDatValue.h"

class TRootLink;
typedef TRootLink* PTRootLink;


class TRootLink {
protected:
	PTRootLink pNext;  // указатель на следующее звено
public:
	TRootLink(PTRootLink pN = nullptr) { pNext = pN; }
	PTRootLink  GetNextLink() { return  pNext; }
	void SetNextLink(PTRootLink  pLink) { pNext = pLink; }
	void InsNextLink(PTRootLink  pLink) 
	{
		PTRootLink p = pNext;  pNext = pLink;
		if (pLink != nullptr) pLink->pNext = p;
	}
	virtual void       SetDatValue(PTDatValue pVal) = 0;
	virtual PTDatValue GetDatValue() = 0;

	friend class TDatList;
};

```

###Класс TDatLink
####Файл TDatLink.h

```C++
#pragma once
#include "TRootLink.h"

class TDatLink;
typedef TDatLink *PTDatLink;

class TDatLink : public TRootLink {
protected:
	PTDatValue pValue; 
public:
	TDatLink(PTDatValue pVal = nullptr, PTRootLink pN = nullptr) : TRootLink(pN) 
	{
		pValue = pVal;
	}
	void       SetDatValue(PTDatValue pVal) { pValue = pVal; }
	PTDatValue GetDatValue() { return  pValue; }
	PTDatLink  GetNextDatLink() { return  (PTDatLink)pNext; }
	friend class TDatList;
};

```

###Класс TDatList
####Файл TDatList.h
```C++
#pragma once
#include "TDatLink.h"

enum LINKPOS { FIRST, CURRENT, LAST };


class TDatList 
{
protected:
	PTDatLink pFirst;															// первое звено
	PTDatLink pLast;															// последнее звено
	PTDatLink pCurrLink;														// текущее звено
	PTDatLink pPrevLink;														// звено перед текущим
	PTDatLink pStop;															// значение указателя, означающего конец списка 
	int CurrPos;																// номер текущего звена
	int ListLen;																// количество звеньев в списке 
	PTDatLink GetLink(PTDatValue pVal = nullptr, PTDatLink pLink = nullptr);
	void      DelLink(PTDatLink pLink);											// удаление звена
public:
	TDatList();
	~TDatList() { DelList(); }
	PTDatValue GetDatValue(LINKPOS mode = CURRENT) const;						// Значение
	virtual int IsEmpty()  const { return pFirst == pStop; }					// Проверка на пустоту
	int GetListLength()    const { return ListLen; }							// Количество звеньев
	int SetCurrentPos(int pos);													// Установить текущее звено
	int GetCurrentPos(void) const;												// Получить номер тек. звена
	virtual int Reset(void);													// Установить на начало списка
	virtual int IsListEnded(void) const;										// Проверка на завершения списка
	int GoNext(void);															// сдвиг вправо текущего звена
	virtual void InsFirst(PTDatValue pVal = nullptr);							// вставить перед первымGe
	virtual void InsLast(PTDatValue pVal = nullptr);							// вставить последним 
	virtual void InsCurrent(PTDatValue pVal = nullptr);							// вставить перед текущим 
	virtual void DelFirst(void);												// удалить первое звено 
	virtual void DelCurrent(void);												// удалить текущее звено 
	virtual void DelList(void);													// удалить весь список
};

```

####Файл TDatList.cpp

```C++
#include "TDatList.h"

PTDatLink TDatList::GetLink(PTDatValue pVal, PTDatLink pLink)
{
	return new TDatLink(pVal, pLink);
}

void TDatList::DelLink(PTDatLink pLink)
{
	if (pLink != nullptr)
	{
		if (pLink->pValue != nullptr)
			delete pLink->pValue;
		delete pLink;
	}
}

TDatList::TDatList(): ListLen(0)
{
	pFirst = pLast = pStop = nullptr;
	Reset();
}

PTDatValue TDatList::GetDatValue(LINKPOS mode) const
{
	PTDatLink temp = nullptr;
	switch (mode)
	{
		case FIRST: 
			temp = pFirst; 
			break;
		case CURRENT: 
			temp = pCurrLink; 
			break;
		case LAST: 
			temp = pLast; 
			break;
	}
	return (temp == nullptr) ? nullptr : temp->GetDatValue();
}



int TDatList::Reset()
{
	pPrevLink = pStop;
	if (IsEmpty()) 
	{
		CurrPos = -1;
		pCurrLink = pStop;
	}
	else 
	{
		CurrPos = 0;
		pCurrLink = pFirst;
	}
	return 1;
}

int TDatList::SetCurrentPos(int pos)
{
	if (pos >= ListLen)
		return 0;
	Reset();
	for (int i = 0; i < pos; GoNext(), i++);
	return 1;
}

int TDatList::GetCurrentPos() const
{
	return CurrPos;
}


int TDatList::IsListEnded() const
{
	return (pCurrLink == pStop);
}

int TDatList::GoNext()
{
	if (IsListEnded())
		return 0;
	else 
	{
		pPrevLink = pCurrLink;
		pCurrLink = pCurrLink->GetNextDatLink();
		CurrPos++;
		return 1;
	}
}

void TDatList::InsFirst(PTDatValue pVal)
{
	PTDatLink DatLink = GetLink(pVal, pFirst);
	if (DatLink != nullptr)
	{
		pFirst = DatLink;
		ListLen++;
		if (ListLen == 1)
		{
			pLast = DatLink;
			Reset();
		}
		else if (CurrPos == 0) 
		{
			pCurrLink = DatLink;
		}
		else
			CurrPos++;
	}
}

void TDatList::InsLast(PTDatValue pVal) 
{
	PTDatLink DatLink = GetLink(pVal, pStop);
	if (DatLink != nullptr)
	{
		if (pLast)
			pLast->SetNextLink(DatLink);
		pLast = DatLink;
		ListLen++;
		if (ListLen == 1)
		{
			pFirst = DatLink;
			Reset();
		}
		if (IsListEnded())
			pCurrLink = DatLink;
	}
}

void TDatList::InsCurrent(PTDatValue pVal )
{
	if ((pCurrLink == pFirst) || IsEmpty())
		InsFirst(pVal);
	else if (IsListEnded())
		InsLast(pVal);
	else
	{
		PTDatLink DatLink = GetLink(pVal, pCurrLink);
		if (DatLink != nullptr)
		{
			pPrevLink->SetNextLink(DatLink);
			DatLink->SetNextLink(pCurrLink);
			pCurrLink = DatLink;
			ListLen++;
		}
	}
}

void TDatList::DelFirst() 
{
	if (!IsEmpty())
	{
		PTDatLink temp = pFirst;
		pFirst = pFirst->GetNextDatLink();
		DelLink(temp);
		ListLen--;
		if (IsEmpty())
		{
			pLast = pStop;
			Reset();
		}
		else if (CurrPos == 0) pCurrLink = pFirst;
		else if (CurrPos == 1) pPrevLink = pStop;
		if (CurrPos) CurrPos--;
	}
}

void TDatList::DelCurrent() 
{
	if (pCurrLink)
	{
		if ((pCurrLink == pFirst) || IsEmpty())
			DelFirst();
		else
		{
			PTDatLink temp = pCurrLink;
			pCurrLink = pCurrLink->GetNextDatLink();
			pPrevLink->SetNextLink(pCurrLink);
			DelLink(temp);
			ListLen--;
			if (pCurrLink == pLast)
			{
				pLast = pPrevLink;
				pCurrLink = pStop;
			}
		}
	}
}

void TDatList::DelList()
{
	while(!IsEmpty()) DelFirst();
	pFirst = pLast = pPrevLink = pCurrLink = pCurrLink = pStop;
	CurrPos = -1;
}

```

###Класс THeadRing
####Файл THeadRing.h

```C++
#pragma once
#include "TDatList.h"

class THeadRing : public TDatList 
{
protected:
	PTDatLink pHead;     // заголовок, pFirst - звено за pHead
public:
	THeadRing();
	~THeadRing();

	virtual void InsFirst(PTDatValue pVal = nullptr); // после заголовка											   
	virtual void DelFirst(void);					  // удалить первое звено
};

```

####Файл THeadRing.cpp

```C++
#include "THeadRing.h"

THeadRing::THeadRing(): TDatList()
{
	InsLast();
	pHead = pFirst;
	pStop = pHead;
	Reset();
	ListLen = 0;
	pFirst->SetNextLink(pFirst);
}

THeadRing:: ~THeadRing()
{
	DelList();
	DelLink(pHead);
	pHead = nullptr;
}

void THeadRing::InsFirst(PTDatValue pVal)
{
	TDatList::InsFirst(pVal);
	pHead->SetNextLink(pFirst);
}

void THeadRing::DelFirst()
{
	TDatList::DelFirst();
	pHead->SetNextLink(pFirst);
}
```

##Разработка структуры хранения полиномов

###Класс TMonom
####Файл TMonom.h

```C++
#pragma once
#include "TDatValue.h"
#include <iostream>
#include <math.h>
using namespace std;

class TMonom;
typedef TMonom* PTMonom;

class TMonom : public TDatValue {
protected:
	int Coeff; // коэффициент монома
	int Index; // индекс (свертка степеней)
public:
	TMonom(int cval = 1, int ival = 0) 
	{
		Coeff = cval; Index = ival;
	};
	virtual TDatValue * GetCopy() { return new TMonom(Coeff, Index); } // изготовить копию
	void SetCoeff(int cval) { Coeff = cval; }
	int  GetCoeff(void) { return Coeff; }
	void SetIndex(int ival) { Index = ival; }
	int  GetIndex(void) { return Index; }
	TMonom& operator=(const TMonom &tm) 
	{
		Coeff = tm.Coeff; Index = tm.Index;
		return *this;
	}
	bool operator==(const TMonom &tm) const
	{
		return (Coeff == tm.Coeff) && (Index == tm.Index);
	}

	bool operator!=(const TMonom &tm) const
	{
		return !operator==(tm);
	}

	bool operator<(const TMonom &tm) const
	{
		return Index < tm.Index;
	}

	bool operator>(const TMonom &tm) const
	{
		return Index > tm.Index;
	}

	bool operator<=(const TMonom &tm) const
	{
		return Index <= tm.Index;
	}

	bool operator>=(const TMonom &tm) const
	{
		return Index >= tm.Index;
	}

	friend ostream& operator<<(ostream &os, TMonom &tm) 
	{
		if (tm.Coeff < 0)
			os << tm.Coeff;
		else if (tm.Coeff > 0)
			os << "+" << tm.Coeff;
		else
			return os;
		if (tm.Index >= 100)
		{
			int X, Y, Z, index = tm.Index;
			Z = index % 10;
			index = floor((double)index / 10);
			Y = index % 10;
			index = floor((double)index / 10);
			X = index % 10;
			index = floor((double)index / 10);
			if (X != 0)
				os << "X^" << X;
			if (Y != 0)
				os << "Y^" << Y;
			if (Z != 0)
				os << "Z^" << Z;
		}
		else if (tm.Index >= 10)
		{
			int X, Y, index = tm.Index;
			Y = index % 10;
			index = floor((double)index / 10);
			X = index % 10;
			index = floor((double)index / 10);
			if (X != 0)
				os << "X^" << X;
			if (Y != 0)
				os << "Y^" << Y;
		}
		else if (tm.Index > 0)
		{
			int X, index = tm.Index;
			X = index % 10;
			index = floor((double)index / 10);
			if (X != 0)
				os << "X^" << X;
		}
		else
		{

		}
		return os;
	}
	friend class TPolinom;
};

```

###Класс TPolinom
####Файл TPolinom.h

```C++
#pragma once
#include "THeadRing.h"
#include "TMonom.h"

#include <iostream>
using namespace std;

class TPolinom : public THeadRing 
{
public:
	TPolinom(int monoms[][2] = nullptr, int km = 0);		// конструктор
	TPolinom(TPolinom &q);									// конструктор копирования
	PTMonom  GetMonom() { return (PTMonom)GetDatValue(); }
	TPolinom & operator+(TPolinom &q);						// сложение полиномов
	TPolinom & operator-(TPolinom &q);						// вычитание полиномов
	TPolinom & operator=(TPolinom &q);						// присваивание
	bool operator==(TPolinom &q);                           // сравнение

	friend ostream& operator<<(ostream &os, TPolinom &q);
};

```

####Файл TPolinom.cpp

```C++
#include "TPolinom.h"

TPolinom::TPolinom(int monoms[][2], int km)
{
	PTMonom Monom = new TMonom(0, -1);
	pHead->SetDatValue(Monom);
	for (int i = 0; i < km; i++)
	{
		Monom = new TMonom(monoms[i][0], monoms[i][1]);
		InsLast(Monom);
	}
}

TPolinom::TPolinom(TPolinom &q)
{
	PTMonom Monom = new TMonom(0, -1);
	pHead->SetDatValue(Monom);
	for (q.Reset(); !q.IsListEnded(); q.GoNext())
	{
		Monom = q.GetMonom();
		InsLast(Monom->GetCopy());
	}
}

TPolinom& TPolinom:: operator+(TPolinom &q)
{
	PTMonom pm, qm, rm;
	q.Reset();
	Reset();

	while (true)
	{
		pm = GetMonom();
		qm = q.GetMonom();
		if (pm->Index < qm->Index)
		{
			rm = new TMonom(qm->Coeff, qm->Index);
			InsCurrent(rm);
			q.GoNext();
		}
		else if(pm->Index > qm->Index)
		{
			GoNext();
		}
		else 
		{
			if (pm->Index == -1)
				break;
			pm->Coeff += qm->Coeff;
			if (pm->Coeff != 0) 
			{
				GoNext();
				q.GoNext();
			}
			else 
			{
				DelCurrent();
				q.GoNext();
			}
		}
	}
	return *this;
}

TPolinom& TPolinom:: operator-(TPolinom &q)
{
	PTMonom pm, qm, rm;
	q.Reset();
	Reset();

	while (true)
	{
		pm = GetMonom();
		qm = q.GetMonom();
		if (pm->Index < qm->Index)
		{
			rm = new TMonom(qm->Coeff, qm->Index);
			InsCurrent(rm);
			q.GoNext();
		}
		else if (pm->Index > qm->Index)
		{
			GoNext();
		}
		else
		{
			if (pm->Index == -1)
				break;
			pm->Coeff -= qm->Coeff;
			if (pm->Coeff != 0)
			{
				GoNext();
				q.GoNext();
			}
			else
			{
				DelCurrent();
				q.GoNext();
			}
		}
	}
	return *this;
}

TPolinom & TPolinom:: operator=(TPolinom &q)
{
	DelList();

	if ( (&q!=nullptr) && (&q != this) )
	{
		PTMonom Mon = new TMonom(0, -1);
		pHead->SetDatValue(Mon);
		for (q.Reset(); !q.IsListEnded(); q.GoNext())
		{
			Mon = q.GetMonom();
			InsLast(Mon->GetCopy());
		}
	}
	return *this;
}

bool TPolinom::operator==(TPolinom &q)
{
	TPolinom Polinom(q);
	int PositionBuffer = GetCurrentPos();
	SetCurrentPos(0);
	Polinom.SetCurrentPos(0);
	for (int i = 0; i < ListLen; i++)
	{
		if (*Polinom.GetMonom() != *GetMonom())
		{
			SetCurrentPos(PositionBuffer);
			return false;
		}
		GoNext();
		Polinom.GoNext();
	}
	SetCurrentPos(PositionBuffer);
	return true;
}

ostream& operator<<(ostream &os, TPolinom &q)
{
	int PositionBuffer = q.GetCurrentPos();
	for (q.Reset(); !q.IsListEnded(); q.GoNext())
		cout << *q.GetMonom();
	os << endl;
	q.SetCurrentPos(PositionBuffer);
	return os;
}
```

##Проверка работоспособности написанных классов с помощью Google Test Framework

###Тесты для класса TMonom
```C++
#include "../Project1/gtest.h"
#include "../Poly-2/TMonom.h"

TEST(TMonom, can_create_monom)
{
	ASSERT_NO_THROW(TMonom Monom1());
	ASSERT_NO_THROW(TMonom Monom1(5));
	ASSERT_NO_THROW(TMonom Monom1(5, 243));
}

TEST(TMonom, can_get_coeff)
{
	TMonom Monom1(25, 243);
	EXPECT_EQ(25, Monom1.GetCoeff());
}

TEST(TMonom, can_set_coeff)
{
	TMonom Monom1(5, 243);
	Monom1.SetCoeff(25);
	EXPECT_EQ(25, Monom1.GetCoeff());
}

TEST(TMonom, can_get_index)
{
	TMonom Monom1(5, 243);
	EXPECT_EQ(243, Monom1.GetIndex());
}

TEST(TMonom, can_set_index)
{
	TMonom Monom1(5, 23);
	Monom1.SetIndex(243);
	EXPECT_EQ(243, Monom1.GetIndex());
}

TEST(TMonom, can_equate_monoms)
{
	TMonom Monom1(5, 23);
	TMonom Monom2(14, 142);
	Monom2 = Monom1;
	EXPECT_EQ(5, Monom2.GetCoeff());
	EXPECT_EQ(23, Monom2.GetIndex());
}

TEST(TMonom, can_equale_monoms)
{
	TMonom Monom1(5, 23);
	TMonom Monom2(14, 142);
	TMonom Monom3(5, 23);
	EXPECT_TRUE(Monom1 == Monom3);
	EXPECT_FALSE(Monom1 == Monom2);
}

TEST(TMonom, can_not_equale_monoms)
{
	TMonom Monom1(5, 23);
	TMonom Monom2(14, 142);
	TMonom Monom3(5, 23);
	EXPECT_FALSE(Monom1 != Monom3);
	EXPECT_TRUE(Monom1 != Monom2);
}

TEST(TMonom, can_compare_monoms)
{
	TMonom Monom1(5, 23);
	TMonom Monom2(14, 142);
	TMonom Monom3(2, 1);
	TMonom Monom4(5, 23);
	EXPECT_TRUE(Monom1 > Monom3);
	EXPECT_TRUE(Monom1 < Monom2);
	EXPECT_TRUE(Monom1 >= Monom4);
	EXPECT_TRUE(Monom1 <= Monom4);
	EXPECT_FALSE(Monom3 > Monom1);
	EXPECT_FALSE(Monom2 < Monom1);
}

```

###Тесты для класса TPolinom

```C++
#include "../Project1/gtest.h"
#include "../Poly-2/TPolinom.h"

TEST(TPolinom, can_create_polinom)
{
	ASSERT_NO_THROW(TPolinom Polinom1());
	int ms[][2] = { { 1, 543 },{ 3, 432 },{ 5, 321 },{ 7, 210 },{ 9, 100 } };
	int mn = sizeof(ms) / (2 * sizeof(int));
	ASSERT_NO_THROW(TPolinom Polinom2(ms, mn));
}

TEST(TPolinom, can_get_right_check_to_empty)
{
	TPolinom Polinom;
	TMonom Monom(2, 24);
	EXPECT_TRUE(Polinom.IsEmpty());
	Polinom.InsCurrent(Monom.GetCopy());
	EXPECT_FALSE(Polinom.IsEmpty());
}

TEST(TPolinom, can_get_monom)
{
	int ms[][2] = { { 1, 543 },{ 3, 432 },{ 5, 321 },{ 7, 210 },{ 9, 100 } };
	int mn = sizeof(ms) / (2 * sizeof(int));
	TPolinom Polinom1(ms, mn);
	TMonom Monoms[5];
	for (int i = 0; i < 5; i++)
	{
		Monoms[i] = TMonom(ms[i][0], ms[i][1]);
	}
	for (int i = 0; i < 5; i++)
	{
		EXPECT_TRUE(Monoms[i] == *Polinom1.GetMonom());
		Polinom1.GoNext();
	}
}

TEST(TPolinom, can_get_list_length)
{
	int ms[][2] = { { 1, 543 },{ 3, 432 },{ 5, 321 },{ 7, 210 },{ 9, 100 } };
	int mn = sizeof(ms) / (2 * sizeof(int));
	TPolinom Polinom1(ms, mn);
	EXPECT_EQ(5, Polinom1.GetListLength());
}

TEST(TPolinom, can_set_curret_pos)
{
	int ms[][2] = { { 1, 543 },{ 3, 432 },{ 5, 321 },{ 7, 210 },{ 9, 100 } };
	int mn = sizeof(ms) / (2 * sizeof(int));
	TPolinom Polinom1(ms, mn);
	Polinom1.SetCurrentPos(3);
	EXPECT_EQ(7, Polinom1.GetMonom()->GetCoeff());
	EXPECT_EQ(210, Polinom1.GetMonom()->GetIndex());
}

TEST(TPolinom, can_get_curret_pos)
{
	int ms[][2] = { { 1, 543 },{ 3, 432 },{ 5, 321 },{ 7, 210 },{ 9, 100 } };
	int mn = sizeof(ms) / (2 * sizeof(int));
	TPolinom Polinom1(ms, mn);
	Polinom1.SetCurrentPos(3);
	EXPECT_EQ(3, Polinom1.GetCurrentPos());
}

TEST(TPolinom, can_get_right_is_list_ended)
{
	int ms[][2] = { { 1, 543 },{ 3, 432 },{ 5, 321 },{ 7, 210 },{ 9, 100 } };
	int mn = sizeof(ms) / (2 * sizeof(int));
	TPolinom Polinom1(ms, mn);
	for (int i = 0; i < 5; i++)
	{
		EXPECT_FALSE(Polinom1.IsListEnded());
		Polinom1.GoNext();
	}
	EXPECT_TRUE(Polinom1.IsListEnded());
}

TEST(TPolinom, can_go_next)
{
	int ms[][2] = { { 1, 543 },{ 3, 432 },{ 5, 321 },{ 7, 210 },{ 9, 100 } };
	int mn = sizeof(ms) / (2 * sizeof(int));
	TPolinom Polinom1(ms, mn);
	for (int i = 0; i < 4; i++)
		Polinom1.GoNext();
	EXPECT_EQ(9, Polinom1.GetMonom()->GetCoeff());
}

TEST(TPolinom, can_insert_first)
{
	int ms[][2] = { { 1, 543 },{ 3, 432 },{ 5, 321 },{ 7, 210 },{ 9, 100 } };
	int mn = sizeof(ms) / (2 * sizeof(int));
	TPolinom Polinom1(ms, mn);
	TMonom monom(2, 100);
	Polinom1.InsFirst(monom.GetCopy());
	EXPECT_TRUE(monom == *Polinom1.GetMonom());
}

TEST(TPolinom, can_insert_last)
{
	int ms[][2] = { { 1, 543 },{ 3, 432 },{ 5, 321 },{ 7, 210 },{ 9, 100 } };
	int mn = sizeof(ms) / (2 * sizeof(int));
	TPolinom Polinom1(ms, mn);
	TMonom monom(2, 100);
	Polinom1.InsLast(monom.GetCopy());
	Polinom1.SetCurrentPos(Polinom1.GetListLength() - 1);
	EXPECT_TRUE(*Polinom1.GetMonom() == monom);
}

TEST(TPolinom, can_insert)
{
	int ms[][2] = { { 1, 543 },{ 3, 432 },{ 5, 321 },{ 7, 210 },{ 9, 100 } };
	int mn = sizeof(ms) / (2 * sizeof(int));
	TPolinom Polinom1(ms, mn);
	TMonom monom(2, 100);
	Polinom1.InsCurrent(monom.GetCopy());
	EXPECT_TRUE(*Polinom1.GetMonom() == monom);
}

TEST(TPolinom, can_get_right_summ)
{
	int ms1[][2] = { { 1, 543 },{ 3, 432 } };
	int mn1 = sizeof(ms1) / (2 * sizeof(int));
	TPolinom Polinom1(ms1, mn1);
	int ms2[][2] = { {2,543},{3, 432} };
	int mn2 = sizeof(ms2) / (2 * sizeof(int));
	TPolinom Polinom2(ms2, mn2);
	TPolinom Polinom3;
	Polinom3 = Polinom1 + Polinom2;
	TMonom Monom1(3, 543);
	TMonom Monom2(6, 432);
	EXPECT_TRUE(Monom1 == *Polinom3.GetMonom());
	Polinom3.GoNext();
	EXPECT_TRUE(Monom2 == *Polinom3.GetMonom());
}

TEST(TPolinom, can_get_right_equale)
{
	int ms1[][2] = { { 1, 543 },{ 3, 432 } };
	int mn1 = sizeof(ms1) / (2 * sizeof(int));
	TPolinom Polinom1(ms1, mn1);
	TPolinom Polinom2(ms1, mn1);
	TPolinom Polinom3;
	EXPECT_TRUE(Polinom1 == Polinom2);
	EXPECT_FALSE(Polinom1 == Polinom3);
}
```

###Результаты тестов

![tests](image/tests.png)

##Разработка тестового приложения

###Класс PolynomReader

####Файл PolynomReader.h

```C++
#pragma once
#include "TPolinom.h"
#include <iostream>
#include <string>
#include <map>

class PolynomReader
{
protected:
	int StrToIndex(string str)
	{
		int index = 0;
		if (str.find_first_of('X') != -1)
		{
			int x_pointer = str.find_first_of('X') + 2;
			index += 100 * (int)(str[x_pointer] - '0');
		}
		if (str.find_first_of('Y') != -1)
		{
			int y_pointer = str.find_first_of('Y') + 2;
			index += 10 * (int)(str[y_pointer] - '0');
		}
		if (str.find_first_of('Z') != -1)
		{
			int z_pointer = str.find_first_of('Z') + 2;
			index += (int)(str[z_pointer] - '0');
		}
		return index;
	}
	int StrToInt(string str)
	{
		int buffer = 0, f;
		for (int i = (int)str.length() - 1,f = 0; i >= 0; i--, f++)
			buffer += (int)(str[i] - '0') * pow(10, f);
		return buffer;
	}
public:
	TPolinom Read(string str)
	{
		map<int, int> Data;
		int buffer = 1, i = 0;
		if (str[0] == '-')
		{
			buffer = -1;
			i = 1;
		}
		while (i < str.length())
		{
			if ((str[i] >= '1' && str[i] <= '9') || str[i] == 'X' || str[i] == 'Y' || str[i] == 'Z')
			{
				int end = str.find_first_of('-', i);
				int coeff;
				int index = 0;
				if (end == -1 || str.find_first_of('+', i) < end)
					end = str.find_first_of('+', i);
				if (end == -1)
				{
					end = str.length();
				}
				if (str[i] >= '1' && str[i] <= '9')
				{
					int NumEnd = str.find_first_of('X', i);
					if (NumEnd == -1 || NumEnd > end)
						NumEnd = str.find_first_of('Y', i);
					if (NumEnd == -1 || NumEnd > end)
						NumEnd = str.find_first_of('Z', i);
					if (NumEnd == -1)
					{
						index = -1;
						NumEnd = end;
					}
					coeff = StrToInt(str.substr(i, NumEnd - i));
					i = NumEnd;
				}
				else
					coeff = 1;
				if (index != -1)
					index = StrToIndex(str.substr(i, end - i));
				else
					index = 0;
				i = end + 1;
				if (Data.find(index) == Data.end())
					Data[index] = coeff;
				else
					Data[index] += coeff;
			}
			else
				i++;
		}
		TPolinom result;
		for each(pair<int, int> c in Data)
		{
			TMonom monom(c.second, c.first);
			result.InsLast(monom.GetCopy());
		}
		return result;
	}
};
```

###Функция Main

```C++
#include "PolynomReader.h"
#include <iostream>

using namespace std;

int main()
{
	string polinom1, polinom2;
	PolynomReader reader;
	cout << "write exit or e or quit or q to exit" << endl;
	while (true)
	{
		cout << "write polinom number one (like 15+2X^2Y^4Z^2+...):" << endl;
		cin >> polinom1;
		if (polinom1 == "exit" || polinom1 == "e" || polinom1 == "quit" || polinom1 == "q")
			return 0;
		TPolinom Pol1(reader.Read(polinom1));
		cout << "write polinom number two (like 15+2X^2Y^4Z^2+...):" << endl;
		cin >> polinom2;
		if (polinom2 == "exit" || polinom2 == "e" || polinom2 == "quit" || polinom2 == "q")
			return 0;
		TPolinom Pol2(reader.Read(polinom2));
		cout << (Pol1 + Pol2) << endl;
	}

	return 0;
}
```

###Результат работы тестового приложения

![test_programm](image/test_programm.png)

##Выводы

В ходе выполнения данной работы были получены навыки создания структуры данных список (TDatList) и организации работы с полиномами на основе списков (TPolinom).

Функциональность написанной системы была протестирована при помощи Google Test Framework. Тесты показали, что разработанная программа успешно решает поставленную в начале работы задачу.