// Copyright(c) 2017 Aleksandr Nazarov
#pragma once

template <class DataType>
class DatLink
{

protected:

	DataType Data;														// ������
	DatLink<DataType>* Link;											// ��������� �� ��������� DatLink

public:

	DatLink(DataType Data = NULL, DatLink<DataType> *Link = nullptr)	// �����������
	{
		this->Data = Data;
		this->Link = Link;
	}
	DataType GetData() { return Data; }									// ����� ������
	DataType& GetLinkData() { return Data; }							// ����� ������ �� ������
	void PutData(DataType dat) { Data = dat; }							// �������� ������
	DatLink<DataType>* GetLink() { return Link; }						// ����� ���������
	void PutLink(DatLink<DataType> *Link) { this->Link = Link; }		// �������� ��������� 
	bool IsLinked() { return (Link == nullptr) ? false : true; }		// �������� ������������� ���������
	bool IsDataExist() { return (Data == NULL) ? false : true; }		// �������� ������������� ������
};