// Copyright(c) 2017 Aleksandr Nazarov
#pragma once

#include "DatLink.h"

template<class DataType>
class DatList
{
private: 

	void FirstAdd(DataType Data);								// ���������� ������� �������� ������
	void CleanDatList();										// ������� �����

protected:

	DatLink<DataType>* pFirst;									// ��������� �� ������ ��������
	DatLink<DataType>* pLast;									// ��������� �� ��������� ��������
	DatLink<DataType>* pCurrLink;								// ��������� �� ������� ��������
	DatLink<DataType>* pPrevLink;								// ��������� �� ���������� �������� 
	int CurrPos;													// �������
	int Count;													// ���������� ������

public:

	DatList();													// �����������
	DatList(const DatList<DataType>& DatLi);                     // ����������� �����������
	~DatList();													// ����������
	int GetCount() { return Count; }							// ������� ���������� ������
	bool IsEmpty() { return !(bool)Count; }						// �������� �� �������
	DataType GetData() { return pCurrLink->GetData(); }			// ������� ������
	void Replace(DataType Data) { pCurrLink->PutData(Data); }	// �������� ������ �� ������� ��������
	void AddToEnd(DataType Data);                               // �������� ������ � �����
	void AddToBegin(DataType Data);                             // �������� ������ � ������
	void Add(DataType Data);                                    // �������� ������ � ������� �������
	void Remove();												// ������� ������� ����
	void RemoveLast();											// ������� ��������� ����
	void RemoveFirst();											// ������� ������ ����
	void Clean();
	DataType operator++();                                     // ���������� �������� ����������
	DataType operator++(DataType);                              // ����������� �������� ����������
	DatList<DataType>& operator=(const DataType& data);			// �������� ������������ ������
	DataType& operator[](int i);                                // �������� �������
	DataType NextLink() { return ++(*this); }                   // �������� ���������� � ���� �������
};


template<class DataType>
DatList<DataType>::DatList()
{
	pFirst = nullptr;
	pLast = nullptr;
	pCurrLink = nullptr;
	pPrevLink = nullptr;
	CurrPos = -1;
	Count = 0;
}

template<class DataType>
DatList<DataType>::DatList(const DatList<DataType>& DatLi)
{
	CleanDatList();
	for (i = 0; i < DatLi.Count; i++)
		AddToEnd(DatLi[i]);
}

template<class DataType>
DatList<DataType>::~DatList()
{
	for (int i = 0; i < Count; i++)
	{
		pCurrLink = pFirst->GetLink();
		delete[] pFirst;
		pFirst = pCurrLink;
	}
}

template<class DataType>
void DatList<DataType>::FirstAdd(DataType Data)
{
	DatLink<DataType>* Link = new DatLink<DataType>(Data);
	Link->PutLink(Link);
	pFirst = Link;
	pLast = Link;
	pCurrLink = Link;
	pPrevLink = Link;
	CurrPos++;
	Count++;
}

template<class DataType>
void DatList<DataType>::CleanDatList()
{
	pFirst = nullptr;
	pLast = nullptr;
	pCurrLink = nullptr;
	pPrevLink = nullptr;
	CurrPos = -1;
	Count = 0;
}

template<class DataType>
void DatList<DataType>::Clean()
{
	for (int i = 0; i < Count; i++)
	{
		pCurrLink = pFirst->GetLink();
		delete[] pFirst;
		pFirst = pCurrLink;
	}
	CleanDatList();
}

template<class DataType>
void DatList<DataType>::AddToEnd(DataType Data)
{
	if (CurrPos > -1)
	{
		DatLink<DataType>* Link = new DatLink<DataType>(Data, pFirst);
		pLast->PutLink(Link);
		pLast = Link;
		Count++;
	}
	else
		FirstAdd(Data);
}

template<class DataType>
void DatList<DataType>::AddToBegin(DataType Data)
{
	if (CurrPos > -1)
	{
		DatLink<DataType>* Link = new DatLink<DataType>(Data, pFirst);
		pFirst = Link;
		pLast->PutLink(Link);
		Count++;
		CurrPos++;
	}
	else
		FirstAdd(Data);
}

template<class DataType>
void DatList<DataType>::Add(DataType Data)
{
	if (CurrPos > -1)
	{
		DatLink<DataType>* Link = nullptr;
		if (pCurrLink == pLast)
			Link = new DatLink<DataType>(Data, pFirst);
		else
			Link = new DatLink<DataType>(Data, pCurrLink->GetLink());
		pCurrLink->PutLink(Link);
		pPrevLink = pCurrLink;
		pCurrLink = Link;
		CurrPos++;
		Count++;
	}
	else
		FirstAdd(Data);
}

template<class DataType>
void DatList<DataType>::Remove()
{
	if (Count > 0)
	{
		if (CurrPos > 0)
		{
			pPrevLink->PutLink(pCurrLink->GetLink());
			delete[] pCurrLink;
			pCurrLink = pPrevLink->GetLink();
			Count--;
		}
		else if (Count > 1)
		{
			pPrevLink = pCurrLink;
			pCurrLink = pCurrLink->GetLink();
			delete[] pPrevLink;
			pPrevLink = pLast;
			pFirst = pCurrLink;
			Count--;
		}
		else
		{
			delete[] pCurrLink;
			CleanDatList();
		}
	}
	else
		throw "nothing to remove";
}

template<class DataType>
void DatList<DataType>::RemoveLast()
{
	if (Count > 0)
	{
		if (pLast == pCurrLink)
		{
			Remove();
		}
		else
		{
			DatLink<DataType>* buffer = pFirst;
			for (int i = 0; i < Count - 1; i++)
				buffer = buffer->GetLink();
			delete[] pLast;
			pLast = buffer;
			pLast->PutLink(pFirst);
			Count--;
		}
	}
	else
		throw "nothing to remove";
}

template<class DataType>
void DatList<DataType>::RemoveFirst()
{
	if (Count > 0)
	{
		if (pFirst == pCurrLink)
		{
			Remove();
		}
		else
		{
			pLast->PutLink(pFirst->GetLink());
			delete[] pFirst;
			pFirst = pLast->GetLink();
			Count--;
			CurrPos--;
		}
	}
	else
		throw "nothing to remove";
}

template<class DataType>
DataType DatList<DataType>::operator++()
{
	pCurrLink = pCurrLink->GetLink();
	pPrevLink = pPrevLink->GetLink();
	CurrPos++;
	return pCurrLink->GetData();
}

template<class DataType>
DataType DatList<DataType>::operator++(DataType)
{
	DataType result = pCurrLink->GetData();
	pCurrLink = pCurrLink->GetLink();
	pPrevLink = pPrevLink->GetLink();
	CurrPos++;
	return result;
}

template<class DataType>
DatList<DataType>& DatList<DataType>::operator=(const DataType& data)
{
	pCurrLink->PutData(data);
	return (*this);
}

template<class DataType>
DataType& DatList<DataType>::operator[](int i)
{
	DatLink<DataType>* Link;
	Link = pFirst;
	if (i < Count && i >= 0)
		for (int f = 0; f < i; f++)
			Link = Link->GetLink();
	else
		throw "Incorrect index";
	return Link->GetLinkData();
}