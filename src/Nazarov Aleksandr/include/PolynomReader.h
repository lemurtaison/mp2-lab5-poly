#pragma once
#include "TPolinom.h"
#include <iostream>
#include <string>
#include <map>

class PolynomReader
{
protected:
	int StrToIndex(string str)
	{
		int index = 0;
		if (str.find_first_of('X') != -1)
		{
			int x_pointer = str.find_first_of('X') + 2;
			index += 100 * (int)(str[x_pointer] - '0');
		}
		if (str.find_first_of('Y') != -1)
		{
			int y_pointer = str.find_first_of('Y') + 2;
			index += 10 * (int)(str[y_pointer] - '0');
		}
		if (str.find_first_of('Z') != -1)
		{
			int z_pointer = str.find_first_of('Z') + 2;
			index += (int)(str[z_pointer] - '0');
		}
		return index;
	}
	int StrToInt(string str)
	{
		int buffer = 0, f;
		for (int i = (int)str.length() - 1,f = 0; i >= 0; i--, f++)
			buffer += (int)(str[i] - '0') * pow(10, f);
		return buffer;
	}
public:
	TPolinom Read(string str)
	{
		map<int, int> Data;
		int buffer = 1, i = 0;
		if (str[0] == '-')
		{
			buffer = -1;
			i = 1;
		}
		while (i < str.length())
		{
			if ((str[i] >= '1' && str[i] <= '9') || str[i] == 'X' || str[i] == 'Y' || str[i] == 'Z')
			{
				int end = str.find_first_of('-', i);
				int coeff;
				int index = 0;
				if (end == -1 || str.find_first_of('+', i) < end)
					end = str.find_first_of('+', i);
				if (end == -1)
				{
					end = str.length();
				}
				if (str[i] >= '1' && str[i] <= '9')
				{
					int NumEnd = str.find_first_of('X', i);
					if (NumEnd == -1 || NumEnd > end)
						NumEnd = str.find_first_of('Y', i);
					if (NumEnd == -1 || NumEnd > end)
						NumEnd = str.find_first_of('Z', i);
					if (NumEnd == -1)
					{
						index = -1;
						NumEnd = end;
					}
					coeff = StrToInt(str.substr(i, NumEnd - i));
					i = NumEnd;
				}
				else
					coeff = 1;
				if (index != -1)
					index = StrToIndex(str.substr(i, end - i));
				else
					index = 0;
				i = end + 1;
				if (Data.find(index) == Data.end())
					Data[index] = coeff;
				else
					Data[index] += coeff;
			}
			else
				i++;
		}
		TPolinom result;
		for each(pair<int, int> c in Data)
		{
			TMonom monom(c.second, c.first);
			result.InsLast(monom.GetCopy());
		}
		return result;
	}
};