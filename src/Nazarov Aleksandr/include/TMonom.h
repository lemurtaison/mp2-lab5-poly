#pragma once
#include "TDatValue.h"
#include <iostream>
#include <math.h>
using namespace std;

class TMonom;
typedef TMonom* PTMonom;

class TMonom : public TDatValue {
protected:
	int Coeff; // ����������� ������
	int Index; // ������ (������� ��������)
public:
	TMonom(int cval = 1, int ival = 0) 
	{
		Coeff = cval; Index = ival;
	};
	virtual TDatValue * GetCopy() { return new TMonom(Coeff, Index); } // ���������� �����
	void SetCoeff(int cval) { Coeff = cval; }
	int  GetCoeff(void) { return Coeff; }
	void SetIndex(int ival) { Index = ival; }
	int  GetIndex(void) { return Index; }
	TMonom& operator=(const TMonom &tm) 
	{
		Coeff = tm.Coeff; Index = tm.Index;
		return *this;
	}
	bool operator==(const TMonom &tm) const
	{
		return (Coeff == tm.Coeff) && (Index == tm.Index);
	}

	bool operator!=(const TMonom &tm) const
	{
		return !operator==(tm);
	}

	bool operator<(const TMonom &tm) const
	{
		return Index < tm.Index;
	}

	bool operator>(const TMonom &tm) const
	{
		return Index > tm.Index;
	}

	bool operator<=(const TMonom &tm) const
	{
		return Index <= tm.Index;
	}

	bool operator>=(const TMonom &tm) const
	{
		return Index >= tm.Index;
	}

	friend ostream& operator<<(ostream &os, TMonom &tm) 
	{
		if (tm.Coeff < 0)
			os << tm.Coeff;
		else if (tm.Coeff > 0)
			os << "+" << tm.Coeff;
		else
			return os;
		if (tm.Index >= 100)
		{
			int X, Y, Z, index = tm.Index;
			Z = index % 10;
			index = floor((double)index / 10);
			Y = index % 10;
			index = floor((double)index / 10);
			X = index % 10;
			index = floor((double)index / 10);
			if (X != 0)
				os << "X^" << X;
			if (Y != 0)
				os << "Y^" << Y;
			if (Z != 0)
				os << "Z^" << Z;
		}
		else if (tm.Index >= 10)
		{
			int X, Y, index = tm.Index;
			Y = index % 10;
			index = floor((double)index / 10);
			X = index % 10;
			index = floor((double)index / 10);
			if (X != 0)
				os << "X^" << X;
			if (Y != 0)
				os << "Y^" << Y;
		}
		else if (tm.Index > 0)
		{
			int X, index = tm.Index;
			X = index % 10;
			index = floor((double)index / 10);
			if (X != 0)
				os << "X^" << X;
		}
		else
		{

		}
		return os;
	}
	friend class TPolinom;
};
