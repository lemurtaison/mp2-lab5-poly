#pragma once
#include "THeadRing.h"
#include "TMonom.h"

#include <iostream>
using namespace std;

class TPolinom : public THeadRing 
{
public:
	TPolinom(int monoms[][2] = nullptr, int km = 0);		// конструктор
	TPolinom(TPolinom &q);									// конструктор копирования
	PTMonom  GetMonom() { return (PTMonom)GetDatValue(); }
	TPolinom & operator+(TPolinom &q);						// сложение полиномов
	TPolinom & operator-(TPolinom &q);						// вычитание полиномов
	TPolinom & operator=(TPolinom &q);						// присваивание
	bool operator==(TPolinom &q);                           // сравнение

	friend ostream& operator<<(ostream &os, TPolinom &q);
};
