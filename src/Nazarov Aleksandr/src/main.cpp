#include "PolynomReader.h"
#include <iostream>

using namespace std;

int main()
{
	string polinom1, polinom2;
	PolynomReader reader;
	cout << "write exit or e or quit or q to exit" << endl;
	while (true)
	{
		cout << "write polinom number one (like 15+2X^2Y^4Z^2+...):" << endl;
		cin >> polinom1;
		if (polinom1 == "exit" || polinom1 == "e" || polinom1 == "quit" || polinom1 == "q")
			return 0;
		TPolinom Pol1(reader.Read(polinom1));
		cout << "write polinom number two (like 15+2X^2Y^4Z^2+...):" << endl;
		cin >> polinom2;
		if (polinom2 == "exit" || polinom2 == "e" || polinom2 == "quit" || polinom2 == "q")
			return 0;
		TPolinom Pol2(reader.Read(polinom2));
		cout << (Pol1 + Pol2) << endl;
	}

	return 0;
}