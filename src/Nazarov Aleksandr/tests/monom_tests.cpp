#include "../Project1/gtest.h"
#include "../Poly-2/TMonom.h"

TEST(TMonom, can_create_monom)
{
	ASSERT_NO_THROW(TMonom Monom1());
	ASSERT_NO_THROW(TMonom Monom1(5));
	ASSERT_NO_THROW(TMonom Monom1(5, 243));
}

TEST(TMonom, can_get_coeff)
{
	TMonom Monom1(25, 243);
	EXPECT_EQ(25, Monom1.GetCoeff());
}

TEST(TMonom, can_set_coeff)
{
	TMonom Monom1(5, 243);
	Monom1.SetCoeff(25);
	EXPECT_EQ(25, Monom1.GetCoeff());
}

TEST(TMonom, can_get_index)
{
	TMonom Monom1(5, 243);
	EXPECT_EQ(243, Monom1.GetIndex());
}

TEST(TMonom, can_set_index)
{
	TMonom Monom1(5, 23);
	Monom1.SetIndex(243);
	EXPECT_EQ(243, Monom1.GetIndex());
}

TEST(TMonom, can_equate_monoms)
{
	TMonom Monom1(5, 23);
	TMonom Monom2(14, 142);
	Monom2 = Monom1;
	EXPECT_EQ(5, Monom2.GetCoeff());
	EXPECT_EQ(23, Monom2.GetIndex());
}

TEST(TMonom, can_equale_monoms)
{
	TMonom Monom1(5, 23);
	TMonom Monom2(14, 142);
	TMonom Monom3(5, 23);
	EXPECT_TRUE(Monom1 == Monom3);
	EXPECT_FALSE(Monom1 == Monom2);
}

TEST(TMonom, can_not_equale_monoms)
{
	TMonom Monom1(5, 23);
	TMonom Monom2(14, 142);
	TMonom Monom3(5, 23);
	EXPECT_FALSE(Monom1 != Monom3);
	EXPECT_TRUE(Monom1 != Monom2);
}

TEST(TMonom, can_compare_monoms)
{
	TMonom Monom1(5, 23);
	TMonom Monom2(14, 142);
	TMonom Monom3(2, 1);
	TMonom Monom4(5, 23);
	EXPECT_TRUE(Monom1 > Monom3);
	EXPECT_TRUE(Monom1 < Monom2);
	EXPECT_TRUE(Monom1 >= Monom4);
	EXPECT_TRUE(Monom1 <= Monom4);
	EXPECT_FALSE(Monom3 > Monom1);
	EXPECT_FALSE(Monom2 < Monom1);
}
