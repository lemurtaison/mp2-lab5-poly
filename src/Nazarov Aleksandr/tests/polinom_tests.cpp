#include "../Project1/gtest.h"
#include "../Poly-2/TPolinom.h"

TEST(TPolinom, can_create_polinom)
{
	ASSERT_NO_THROW(TPolinom Polinom1());
	int ms[][2] = { { 1, 543 },{ 3, 432 },{ 5, 321 },{ 7, 210 },{ 9, 100 } };
	int mn = sizeof(ms) / (2 * sizeof(int));
	ASSERT_NO_THROW(TPolinom Polinom2(ms, mn));
}

TEST(TPolinom, can_get_right_check_to_empty)
{
	TPolinom Polinom;
	TMonom Monom(2, 24);
	EXPECT_TRUE(Polinom.IsEmpty());
	Polinom.InsCurrent(Monom.GetCopy());
	EXPECT_FALSE(Polinom.IsEmpty());
}

TEST(TPolinom, can_get_monom)
{
	int ms[][2] = { { 1, 543 },{ 3, 432 },{ 5, 321 },{ 7, 210 },{ 9, 100 } };
	int mn = sizeof(ms) / (2 * sizeof(int));
	TPolinom Polinom1(ms, mn);
	TMonom Monoms[5];
	for (int i = 0; i < 5; i++)
	{
		Monoms[i] = TMonom(ms[i][0], ms[i][1]);
	}
	for (int i = 0; i < 5; i++)
	{
		EXPECT_TRUE(Monoms[i] == *Polinom1.GetMonom());
		Polinom1.GoNext();
	}
}

TEST(TPolinom, can_get_list_length)
{
	int ms[][2] = { { 1, 543 },{ 3, 432 },{ 5, 321 },{ 7, 210 },{ 9, 100 } };
	int mn = sizeof(ms) / (2 * sizeof(int));
	TPolinom Polinom1(ms, mn);
	EXPECT_EQ(5, Polinom1.GetListLength());
}

TEST(TPolinom, can_set_curret_pos)
{
	int ms[][2] = { { 1, 543 },{ 3, 432 },{ 5, 321 },{ 7, 210 },{ 9, 100 } };
	int mn = sizeof(ms) / (2 * sizeof(int));
	TPolinom Polinom1(ms, mn);
	Polinom1.SetCurrentPos(3);
	EXPECT_EQ(7, Polinom1.GetMonom()->GetCoeff());
	EXPECT_EQ(210, Polinom1.GetMonom()->GetIndex());
}

TEST(TPolinom, can_get_curret_pos)
{
	int ms[][2] = { { 1, 543 },{ 3, 432 },{ 5, 321 },{ 7, 210 },{ 9, 100 } };
	int mn = sizeof(ms) / (2 * sizeof(int));
	TPolinom Polinom1(ms, mn);
	Polinom1.SetCurrentPos(3);
	EXPECT_EQ(3, Polinom1.GetCurrentPos());
}

TEST(TPolinom, can_get_right_is_list_ended)
{
	int ms[][2] = { { 1, 543 },{ 3, 432 },{ 5, 321 },{ 7, 210 },{ 9, 100 } };
	int mn = sizeof(ms) / (2 * sizeof(int));
	TPolinom Polinom1(ms, mn);
	for (int i = 0; i < 5; i++)
	{
		EXPECT_FALSE(Polinom1.IsListEnded());
		Polinom1.GoNext();
	}
	EXPECT_TRUE(Polinom1.IsListEnded());
}

TEST(TPolinom, can_go_next)
{
	int ms[][2] = { { 1, 543 },{ 3, 432 },{ 5, 321 },{ 7, 210 },{ 9, 100 } };
	int mn = sizeof(ms) / (2 * sizeof(int));
	TPolinom Polinom1(ms, mn);
	for (int i = 0; i < 4; i++)
		Polinom1.GoNext();
	EXPECT_EQ(9, Polinom1.GetMonom()->GetCoeff());
}

TEST(TPolinom, can_insert_first)
{
	int ms[][2] = { { 1, 543 },{ 3, 432 },{ 5, 321 },{ 7, 210 },{ 9, 100 } };
	int mn = sizeof(ms) / (2 * sizeof(int));
	TPolinom Polinom1(ms, mn);
	TMonom monom(2, 100);
	Polinom1.InsFirst(monom.GetCopy());
	EXPECT_TRUE(monom == *Polinom1.GetMonom());
}

TEST(TPolinom, can_insert_last)
{
	int ms[][2] = { { 1, 543 },{ 3, 432 },{ 5, 321 },{ 7, 210 },{ 9, 100 } };
	int mn = sizeof(ms) / (2 * sizeof(int));
	TPolinom Polinom1(ms, mn);
	TMonom monom(2, 100);
	Polinom1.InsLast(monom.GetCopy());
	Polinom1.SetCurrentPos(Polinom1.GetListLength() - 1);
	EXPECT_TRUE(*Polinom1.GetMonom() == monom);
}

TEST(TPolinom, can_insert)
{
	int ms[][2] = { { 1, 543 },{ 3, 432 },{ 5, 321 },{ 7, 210 },{ 9, 100 } };
	int mn = sizeof(ms) / (2 * sizeof(int));
	TPolinom Polinom1(ms, mn);
	TMonom monom(2, 100);
	Polinom1.InsCurrent(monom.GetCopy());
	EXPECT_TRUE(*Polinom1.GetMonom() == monom);
}

TEST(TPolinom, can_get_right_summ)
{
	int ms1[][2] = { { 1, 543 },{ 3, 432 } };
	int mn1 = sizeof(ms1) / (2 * sizeof(int));
	TPolinom Polinom1(ms1, mn1);
	int ms2[][2] = { {2,543},{3, 432} };
	int mn2 = sizeof(ms2) / (2 * sizeof(int));
	TPolinom Polinom2(ms2, mn2);
	TPolinom Polinom3;
	Polinom3 = Polinom1 + Polinom2;
	TMonom Monom1(3, 543);
	TMonom Monom2(6, 432);
	EXPECT_TRUE(Monom1 == *Polinom3.GetMonom());
	Polinom3.GoNext();
	EXPECT_TRUE(Monom2 == *Polinom3.GetMonom());
}

TEST(TPolinom, can_get_right_equale)
{
	int ms1[][2] = { { 1, 543 },{ 3, 432 } };
	int mn1 = sizeof(ms1) / (2 * sizeof(int));
	TPolinom Polinom1(ms1, mn1);
	TPolinom Polinom2(ms1, mn1);
	TPolinom Polinom3;
	EXPECT_TRUE(Polinom1 == Polinom2);
	EXPECT_FALSE(Polinom1 == Polinom3);
}